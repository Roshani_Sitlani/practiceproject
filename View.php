<!doctype html>
<html lang="en">
  <head>
   
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

     <title>Search</title>
    <link rel="icon" type="image/ico" href="Image/view.jpeg"/>
  </head>
  <body class="overflow-hidden">
   <div class="container" > 
        <form action="success.php" method="POST">   
         <div class="row" style="background-image: url(/practiceproject/Image/img2.jpg); background-position: center;"  >
          <div class="col-lg-12 offset-lg-4" >
 
              <h3 class="p-2 text-primary" style="font-family: 'Impact, Charcoal, sans-serif'; font-size: 60px ;">&nbsp&nbspUser Details</h3>
              <p class="p-1text-alert" style="font-family: 'Verdana';" class="offset-lg-6"></p>
           </div>
          </div>
        <hr class="alert-dark" style="border-width:10px">

  
<?php
$id = $_GET['id'];

$con = new mysqli("localhost", "plus91", "plus91", "userdb");

$result = mysqli_query($con, "Select * from register WHERE uname= '{$id}'");

$rows = mysqli_fetch_array($result);
$fname=$rows['fname'];
$lname=$rows['lname'];
$address=$rows['address'];
$uname=$rows['uname'];
$pno=$rows['phoneno'];
$email=$rows['email'];
$qualification=$rows['qualification'];
$age=$rows['age'];

?>
        
  <div class="row">
    <div class="col-lg-6 offset-lg-3">
      <div class="alert alert-info" style="height: 450px">

          <div class="form-group">
            <div class=row>
              <div class="col-lg-6">
                <label for="firstname" style="font-size: 18px"><b>First Name</b></label>
                <input type="text" name="fname" id="fname" class="form-control" placeholder="Enter First Name" value=<?php echo $fname; ?> required>
              </div>
              <div class="col-lg-6">
                <label for="lastname" style="font-size: 18px"><b>Last Name</b></label>
                <input type="text" name="lname" id="lname" class="form-control" placeholder="Enter Last Name" value=<?php echo $lname; ?> required> 
              </div>
            </div>
          </div>

          <div class="form-group" >
              <label for="address" style="font-size: 18px"><b>Address</b></label>
              <input type="text" name="address" id="address" class="form-control" value=<?php echo $address; ?> placeholder="Enter Address" required> 
            
          </div>  
       
         <div class="form-group" >
             <div class=row>
              <div class="col-lg-6">
                <label for="username" style="font-size: 18px"><b>User Name</b></label>
                <input type="text" class="form-control" name="uname" id="uname" placeholder="Enter Username." value=<?php echo $uname; ?> required>
              </div>
              <div class="col-lg-6">
                <label for="phoneno" style="font-family:'Impact, Charcoal, sans-serif'; font-size: 18px"><b>Phone Number</b></label>
                <input type="text" name="phoneno" id="phoneno" class="form-control" placeholder="Enter Phoneno."  minlength="10" maxlength="10" value=<?php echo $pno; ?>  required>
              </div>
            </div>
          </div>

          <div class="form-group" >
                <label for="email" id="email" style="font-size: 18px"><b>Email</b></label>
                <input type="text" class="form-control" id="email" name="email" value=<?php echo $email; ?> placeholder="Enter email" required>
          </div>

          <div class="form-group" >
             <div class=row>
              <div class="col-lg-6">
                <label for="qualification" style="font-size: 18px"><b>Qualification</b></label>
                <input type="text" class="form-control" name="qualification" id="qualification" placeholder="Enter Qualification." value=<?php echo $qualification; ?> required>
              </div>
              <div class="col-lg-6">
                <label for="age" style="font-size: 18px"><b>Age</b></label>
                <input type="text" name="age" id="age" class="form-control" placeholder="Enter Age."  value=<?php echo $age; ?>  required>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-10 offset-lg-10">
              <a class="btn btn-small btn-dark" href="success.php" name="edit">Back</a>
          </div>
        </div>
      
      </div>
    </div>
    </div>
    

 <hr class="alert-dark " style="border-width:10px">



    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

    </div>
</div>
</div>


</form>
</div>


  </body>
</html>