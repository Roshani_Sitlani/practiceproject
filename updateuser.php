<!doctype html>
<html lang="en">
  <head>
   
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">


   <title>Updateuser Form</title>
    <link rel="icon" type="image/ico" href="Image/update.png"/>
  </head>
  <body class="overflow-hidden">
   <div class="container" > 
        <form action="update.php" method="POST">   
         <div class="row" style="background-image: url(/practiceproject/Image/img2.jpg); background-position: center;"  >
          <div class="col-lg-12 offset-lg-4" >
 
              <h3 class="p-2 text-primary" style="font-family: 'Impact, Charcoal, sans-serif'; font-size: 60px ;">&nbspUpdate Details</h3>
              <p class="p-1text-alert" style="font-family: 'Verdana';" class="offset-lg-6"></p>
           </div>
          </div>
             
           </div>
          </div>
        <hr class="alert-dark" style="border-width:10px">


  
  <?php
$id = $_GET['id'];

$con = new mysqli("localhost", "plus91", "plus91", "userdb");

$result = mysqli_query($con, "Select * from register WHERE uname= '{$id}'");

$rows = mysqli_fetch_array($result);


$fname=$rows['fname'];
$lname=$rows['lname'];
$address=$rows['address'];
$pno=$rows['phoneno'];
$email=$rows['email'];
$qualification=$rows['qualification'];
$age=$rows['age'];
$uname=$rows['uname'];
$pw=$rows['password'];


?>
      <div cloass="form-group">
         <div class="row">
       
         <div class="col-lg-6 offset-lg-3">
         <div class="alert alert-info" style="height: 560px">

          <div class="row" style="margin-top: 7px">
            <div class="col-lg-6">
                <label for="firstname"><b>First Name</b></label>
            </div>
            <div>
                <input type="text" name="fname" id="fname" class="form-control" placeholder="Enter First Name" value="<?php echo $fname;?>" required>
            </div>
          </div>

          <div class="row" style="margin-top: 5px">
            <div class="col-lg-6">
               <label for="lastname"><b>Last Name</b></label>
            </div>
            <div>
              <input type="text" name="lname" id="lname" name="phoneno" class="form-control" placeholder="Enter Last Name" value="<?php echo $lname?>" required>
            </div>
          </div>
  
          <div class="row" style="margin-top: 5px">
            <div class="col-lg-6">
              <label for="address"><b>Address</b></label>
            </div>
            <div>
              <input type="text" name="address" id="address" class="form-control" placeholder="Enter Address" value="<?php echo $address?>" required> 
            </div>  
          </div>  
       
          <div class="row" style="margin-top: 5px">
            <div class="col-lg-6">
              <label for="phoneno"><b>Phone Number</b></label>
            </div>
            <div>
              <input type="text" name="phoneno" id="phoneno" class="form-control" placeholder="Enter Phoneno." minlength="10" maxlength="10" value="<?php echo $pno?>" required>
            </div>
          </div>
  
          <div class="row" style="margin-top: 5px">
            <div class="col-lg-6">
             <label for="email"><b>Email</b></label>
            </div>
            <div>
              <input type="text" name="email" id="email" class="form-control" placeholder="Enter email" value="<?php echo $email?>" required>
            </div>
          </div>

          <div class="row" style="margin-top: 5px">
            <div class="col-lg-6">
             <label for="qualification"><b>Qualification</b></label>
            </div>
            <div>
              <input type="text" name="qualification" id="qualification" class="form-control" placeholder="Enter qualification" value="<?php echo $qualification?>" required>
            </div>
          </div>

          <div class="row" style="margin-top: 5px">
            <div class="col-lg-6">
             <label for="age"><b>Age</b></label>
            </div>
            <div>
              <input type="text" name="age" id="age" class="form-control" placeholder="Enter Age" value="<?php echo $age?>" required>
            </div>
          </div>

          <div class="row" style="margin-top: 5px">
            <div class="col-lg-6">
              <label for="username"><b>User Name</b></label>
            </div>
            <div>
             <input readonly type="text" name="uname" id="uname" class="form-control" placeholder="Enter Username." value="<?php echo $id;?>" required>
            </div>
          </div>

          <div class="row" style="margin-top: 5px">
           <div class="col-lg-6">
            <label for="password"><b>Password</b></label>
           </div>
           <div>
            <input type="password" name="password" id="password" class="form-control" placeholder="Enter password." id="password"  value="<?php echo $pw?>" required>
           </div>
           <div><input type="checkbox" class="form-check-input" id="customCheck1" onclick="showpw(customCheck1)"></div>
         </div>

        <div class="form-group">
         <div class="col-lg-12">
         <br/>
             <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" style="font-family: Comic Sans MS; " for="exampleCheck1">Agree to terms and conditions.</label>
         </div>
         

        </div>
        <div class="row">
          <div class="col-lg-10 offset-lg-8">
            <div><button type="submit" class="btn btn-dark">Update</button>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
    </div>

 <hr class="alert-secondary " style="border-width:8px">
 
  <script type="text/javascript">

   function showpw(customCheck1)
   {

     if(!customCheck1.checked)
     {
       document.getElementById("password").setAttribute("type", "password") ;
     }
     else
     {
       document.getElementById("password").setAttribute("type", "text") ;
     }
   }
   </script>
    <script type="text/javascript">

</script>

  <script type = "text/javascript" >
  function preventBack()
  {
   window.history.forward();
  }
   setTimeout("preventBack()", 0);
   window.onunload=function(){null};
</script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

    </div>
</div>
</div>


</form>
</div>


  </body>
</html>